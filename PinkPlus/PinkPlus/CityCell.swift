//
//  CityCell.swift
//  PinkPlus
//
//  Created by Giap, Le Duc  on 4/15/21.
//

import UIKit

class CityCell: UITableViewCell {

    @IBOutlet weak var nameCity: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
